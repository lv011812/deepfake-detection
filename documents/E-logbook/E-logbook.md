﻿#### 01/10/2021 - Meeting

**Ghantt chart:**

- Design - UML Methodologies
- Implementation
- Add assesments details - change the Ghantt chart assesments dates around the submission dates
  Able to use already existing methods/libraries to create the project

Papers:
Mesonet CNN Paper - https://arxiv.org/pdf/1809.00888.pdf  
Deepfake detection methods - https://ieeexplore.ieee.org/document/9039057  
MAD - Deepfake - https://christoph-busch.de/projects-mad.html


#### 15/10/2021 - Meeting

TensorFlow functions - can I use functions such as Conv2D, flatten?

**Literature Review:**

- What other solutions already exist in regard to what I am doing?
- Why use one over the other?
- Which one will I use and why?

Model & Weights:
Train own weight or to get an already trained weights
Pros/Cons of choosing either?

#### 22/10/2021 - Meeting

Papers: http://ecasp.ece.iit.edu/publications/2012-present/2019-07.pdf
https://paperswithcode.com/paper/can-gan-generated-morphs-threaten-face/review/

#### 12/11/2021 - Meeting

Talking about algorithms, models,
Dataset, benchmarks, data that I can use to implement the algorithm

#### 03/12/2021

finish off literature review, solution approach
design the work, map out and details the methodology

How deepfakes are generated? what methods exist - could be used for introduction/background


### 28/01/2022

Implemtation of alexnet - currently will try for 224 x 224 x3 shape

### 03/04/2022
Completed the UI for the program and is fully functional

### 13/04/2022
Tried implementing ResNet then Xception networks however they are very complex and the time is limited, therefore will only continue with the 2 networks I currently have

### 17/04/2022
The program is fully finished with testing left. 


Papers needed for report.
https://www.eyerys.com/articles/timeline/reddit-user-starts-deepfake#event-a-href-articles-timeline-apple-used-100-recycled-gold-one-its-products-first-timeapple-used-100-recycled-gold-in-one-of-its-products-for-the-first-time-the-company-reported-a - reddit deepfake
https://www.sciencedirect.com/science/article/abs/pii/S0007681319301600 - deepfake example image
https://www.theguardian.com/technology/2020/jan/13/what-are-deepfakes-and-how-can-you-spot-them - deepfake information
https://app.dimensions.ai/discover/publication?search_mode=content&search_text=deepfakes&search_type=kws&search_field=full_search - number of citations

https://arxiv.org/pdf/1406.2661.pdf - gan information
https://machinelearningmastery.com/what-are-generative-adversarial-networks-gans/ - gan information
https://www.simplilearn.com/tutorials/deep-learning-tutorial/perceptron#:~:text=A%20perceptron%20is%20a%20neural,intelligence%20in%20the%20input%20data. - perceptron information

https://arxiv.org/pdf/1610.02357.pdf - Xception model

https://towardsdatascience.com/introduction-to-convolutional-neural-network-cnn-de73f69c5b83 - dense layer information/maxpooling/conv

https://arxiv.org/pdf/1409.4842.pdf - googlenet paper

https://computersciencewiki.org/index.php/Max-pooling_/_Pooling - maxpooling image

https://www.cs.cmu.edu/~rsalakhu/papers/oneshot1.pdf - siamese networks

https://www.kaggle.com/datasets/xhlulu/140k-real-and-fake-faces =dataset

https://medium.com/analytics-vidhya/image-augmentation-9b7be3972e27 data aug example

https://en.wikipedia.org/wiki/AlexNet - alexnet architecture

https://analyticsindiamag.com/what-is-a-convolutional-layer/ - convolution image source